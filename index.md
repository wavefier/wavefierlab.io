---
layout: page
title: Wavefier
subtitle: A prototype for a real time pipeline for the detection of transient signals and their automatic classification..
order: 1
---

![Wavefier Logo](/img/wv_logo.png){: .center-block :}

WaveFier is the result of an industral collaboration project with Trust-IT Srl, Via Francesco Redi 10, 56124, Pisa, Italy and “CNRS - Center National de la Recherche Scientifique in Paris” acting in behalf of the “Laboratori d’Annecy de physique des particules - LAPP UMR n. 51814” carried out in the context of the H2020 Asterics / Obelics project of the European union's Commission Programme.

### Background
The detection of **gravitational waves** has inaugurated the era of gravitational astronomy and opened new challenges for the multi-messenger study of cosmic sources. Thanks to their sensitivity, the Advanced LIGO and Advanced Virgo interferometers will probe a much larger volume of space and expand the capability of discovering new gravitational wave emitters. However, noise identification and its removal remains one of the most challenging problem in GW data analysis. A single GW detector typically produces data with a rate of 7-8 Tb per day with a flux of 40Mb/s. These data have to be analysed in the faster and most efficient way to increase the detection confidence and to obtain information in real time, about likely noise sources and to help the fast alert system for Electromagnetic Follow Up systems.

**Glitches** are transient noise events that can impact the data quality of the interferometers and their classification is an important task. Outlier/noise detection has been studied for decades in time-series analysis. ML methods generally employ a semi-supervised approach, with a few others using supervised or unsupervised techniques. Supervised ML techniques require a training phase with labeled data, in order to learn the data model which classifies outliers and inliers. This can be an expensive operation with massive data since generating a labeled training data set can be time consuming, especially if the data need to be labeled manually. Characterizing the glitches is an important task to reduce the impact of transient noise on the detectors. Inspecting glitches manually is a time-consuming and error-prone task. Furthermore, the increase of sensitivity in advanced detectors will lead to more classes of glitches. The use of machine learning looks a promising way to tackle the classification of glitches.

### Wavefier in action

See below a simple demo of **Wavefier in action**

<!-- blank line -->
<figure class="video_container" style="width: 100%;">
  <video controls="true" allowfullscreen="true" style="width: inherit;">
    <source src="/video/WaveFier.mp4" type="video/mp4">
  </video>
</figure>
<!-- blank line -->

If you want to know better how it is composed Wavefier framework and how it's work, visit the [Pipeline specification](/content/documentation/pipeline/) page

### Why Wavefier?

It would be extremely useful to have an  online pipeline for automatic identification and classification of transient signals for Gravitational Wave detectors and their direct database inclusion.

### Wavefier's goals

The goal of the project developed is setup a prototype for a real time pipeline for the detection of transient signals and their automatic classification. Moreover, the project has the goal to test different software architecture solutions to prototype a scalable pipeline for big data and deep learning analysis in GW context. 

### Key Objectives

- Setup a prototype for a real time pipeline for the detection of transient signals and their automatic classification 
- Best practice for software management 
- Test different software architecture solutions to prototype a scalable pipeline for big data analysis in GW context.
- Interoperability and access to data and services
- ICT services supporting research infrastructures
- Use of data in network infrastructures and services
- Big data and Machine Learning
- Test on Kubernetes cluster






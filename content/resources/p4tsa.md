---
layout: page
title: p4TSA
subtitle: Python package for the time-series analysis
---

The core functionality of WDF rests on the Package for Time Series Analysis library.

p4TSA is a minimal package containing ad hoc function to work with time series. It includes:

- Whitening in time domain
- Wavelet decomposition
- Wavelet Detection Filter

More information about p4TSA, in the dedicated [GitHub repository](https://github.com/elenacuoco/p4TSA)
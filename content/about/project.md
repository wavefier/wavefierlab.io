---
layout: page
title: Wavefier Project
---
WaveFier is the result of an industral collaboration project with Trust-IT Services LTD Chase Side Enfield, Middlesex - EN2 6NF - UK and “CNRS - Center National de la Recherche Scientifique in Paris” acting in behalf of the “Laboratori d’Annecy de physique des particules - LAPP UMR n. 51814” carried out in the context of the H2020 Asterics / Obelics project of the European union’s Commission Programme.
---
layout: page
title: User manual
subtitle: A simple guide to try to use Wavefier for your purpose
---

The documentation about the deployment of Wavefier's component is divided in two part, one for Architectural services (Grafana, InfluxDB, Kafka) and one for Wavefier component

- Wavefier Architectural services deployment: [https://wavefier.gitlab.io/services-kubernetes-deployment-cnaf](https://wavefier.gitlab.io/services-kubernetes-deployment-cnaf){:target="_blank"}

- Wavefier Services deployment: [https://wavefier.gitlab.io/wavefier-kubernetes-deployment](https://wavefier.gitlab.io/wavefier-kubernetes-deployment){:target="_blank"}


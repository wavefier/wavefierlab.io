---
layout: page
title: Wavefier Pipeline
subtitle: How it is composed Wavefier pipeline
---


![Wavefier Pipeline](/img/WAVEFIER_2022.png){: .center-block :}

## Apache Kafka
Open-source stream-processing software platform developed by LinkedIn and donated to the Apache Software Foundation

## Grafana
The output of Wavefier analysys are plotted in several Grafana dashboards. 
**Why Grafana?**
1. Useful build-in features
    - Authentication, Organization and user settings

2. Mixed Datasource, Mix different data sources in the same graph
    - Grafana supports dozens of databases, natively. Mix them together in the same Dashboard.

3. Native Notification and Alerting

## InfluxDB
**Why InfluxDB?**
1. Specific for time series database (TSDB) 
    - All is designed as time series 

2. Friendly because InfluxDB  have a SQL-like query language for interacting with it

3. Grafana has Native support for InfluxDB 

## Docker

## Kubernetes
---
layout: page
title: License
subtitle: Use of Wavefier
---

If you use Wavefier in your scientific publications or projects, we ask that you acknowlege our work by citing the publications that describe Wavefier and the software’s digital object identifier, as described in the Wavefier citation guidelines.

```
GNU GENERAL PUBLIC LICENSE
   Version 3, 29 June 2007
```
